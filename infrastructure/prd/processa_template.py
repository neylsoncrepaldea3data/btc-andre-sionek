import jinja2
#import yaml
import os


def renderiza_template():
    with open('variables.tf.j2', 'r') as f:
        variables_tf = f.read()

    #with open('config.yaml', 'r') as f:
    #    config = yaml.safe_load(f)

    variables_rendered = jinja2.Template(variables_tf)
    variables_rendered = variables_rendered.render({"accountnumber": os.environ["AWS_ACCOUNT_NUMBER"]})

    with open('variables.tf', 'w') as f:
        f.write(variables_rendered)


renderiza_template()