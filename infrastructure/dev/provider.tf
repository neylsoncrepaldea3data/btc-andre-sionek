provider "aws" {
  region = var.aws_region
}

# backend
terraform {
  backend "s3" {
    bucket = "terraform-439543761220"
    key    = "state/btc-ed/dev/terraform.tfstate"
    region = "us-east-1"
  }
}
