resource "aws_lambda_function" "crawlers" {
  filename      = "lambda_function_payload.zip"
  function_name = "${var.lambda_function_name}-${var.environment}"
  role          = aws_iam_role.lambda.arn
  handler       = "lambda_function.handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256("lambda_function_payload.zip")

  runtime = "python3.8"

  environment {
    variables = {
      BUCKET = aws_s3_bucket.dl.id
      REGION = var.aws_region
    }
  }

  tags = {
    projeto = var.mainvalue
  }

}